import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Тестовыйкотикдлялидиидва', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

const catDescription = 'Тестовое описание';


describe('API котиков, добавление описания, метод  save-description', () => {
    beforeAll(async () => {
      try {
        const add_cat_response = await HttpClient.post('core/cats/add', {
          responseType: 'json',
          json: { cats },
        });
        if ((add_cat_response.body as CatsList).cats[0].id) {
          catId = (add_cat_response.body as CatsList).cats[0].id;
        } else throw new Error('Не получилось получить id тестового котика!');
      } catch (error) {
        throw new Error('Не удалось создать котика для автотестов!');
      }
    });
  
    afterAll(async () => {
      await HttpClient.delete(`core/cats/${catId}/remove`, {
        responseType: 'json',
      });
    });
    

  it('Добавление описания котику по существующему id', async () => {
    const response = await HttpClient.post(`core/cats/save-description`, {
        responseType: 'json',
        json: {
            catId: catId,
            catDescription: catDescription,
        }
      });
    expect(response.statusCode).toEqual(200);

    expect(response.body).toMatchObject({
      id: catId,
      description: catDescription,
    });
  });

  
});
